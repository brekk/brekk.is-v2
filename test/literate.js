// # a-literate-js-file.js
/**
// # The slash-double-asterisk above is called a "magic header" and this makes this comment visible in the resulting markdown file.
# h1
_markdown_!
*/
// # use the "=>" magic directive to either:
// # read some other file as a plain document
// => plain include-md.md
// # or read some other file as another literate JS file
// => include include-js.js

const myCode = () => {
  // ostensibly you'd put some real code here that does something
}
