import React from "react"

export const DataContext = React.createContext({})

export const withData = X => props => (
  <DataContext.Consumer>
    {data => <X {...props} {...data} />}
  </DataContext.Consumer>
)
