/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { jsx, css, withTheme } from "./view"
import { fore, back, blackOrWhite } from "./styles"

export const style = p => css`
  color: ${fore(p)};
  cursor: pointer;
  text-decoration: none;
  border-bottom: ${p.noUnderline ? 0 : "1px solid " + fore(p) + ";"}
  transition: color 0.3s ease-out;
  &:hover {
    color: ${blackOrWhite(back(p))};
  }
`

export const Link = props => {
  const { children } = props
  return (
    <a css={style(props)} {...props}>
      {children}
    </a>
  )
}
export default withTheme(Link)
