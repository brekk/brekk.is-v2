/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { jsx, css } from "./view"

import Code from "./Code"

const Install = props => {
  const { owner = "brekk", packageManager = true, pkg, css: parentCSS } = props
  const pack = `${owner === "brekk" ? "" : "@" + owner + '/'}${pkg}`
  return (
    <Code {...props} css={css(parentCSS)}>
      {packageManager ? `yarn add` : `npm i`} {pack}
    </Code>
  )
}
export const WrappedInstall = props => {
  const { className, css: parentCSS } = props
  console.log("PROPS", props)
  return (
    <pre className={className} css={parentCSS}>
      <Install {...props} />
    </pre>
  )
}

export default Install
