/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { jsx, css, withTheme, Box } from "./view"
import { fore, back } from "./styles"
import LinkableHeader from "./LinkableHeader"

const stripFirstNewLine = z => z.replace(/\n/, "")

const decorate = withTheme
const CodeSample = ({
  title = "",
  textAlign = "left",
  children,
  palette,
  theme
}) => (
  <Box>
    <LinkableHeader textAlign={textAlign} fontSize={3}>
      {title}
    </LinkableHeader>
    <Box
      as="pre"
      p={2}
      css={css`
        color: ${back({ theme })};
        background-color: ${fore({ theme })};
        overflow-x: auto;
        text-align: left;
      `}
    >
      <code>{stripFirstNewLine(children)}</code>
    </Box>
  </Box>
)

export default decorate(CodeSample)
