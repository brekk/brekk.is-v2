/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { jsx, css, Box, Heading, withThemeAndData, Text } from "./view"
import Link from "./Link"

const NPMLink = ({ name, description }) => (
  <Box css={css({ flexDirection: "column" })}>
    <Link href={`//npmjs.org/package/${name}`}>{name}</Link>
    <Text py={2}>{description}</Text>
  </Box>
)

const forks = [
  ["throttle-debounce", "throttle and debounce functions"],
  ["navi", "a batteries-included router for React"],
  ["tscodeshift", "codemods for TypeScript"],
  ["get-es-imports-exports", "list all imports and exports in a directory"]
]

export const OSSForks = () => (
  <Box>
    <Heading
      css={css({ textTransform: "uppercase" })}
      title="AKA other people's projects"
    >
      Open-Source Contributions
    </Heading>
    <ul css={css({ padding: 0 })}>
      {forks.map(([x, d], i) => (
        <li key={i} css={css({ listStyle: "none" })}>
          <NPMLink name={x} description={d} />
        </li>
      ))}
    </ul>
  </Box>
)

export default withThemeAndData(OSSForks)
