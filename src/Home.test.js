import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import { addNavigation } from "./testing-utils"

it("renders without crashing", () => {
    const div = document.createElement("div")
    ReactDOM.render(<App navigation={nav} />, div)
    ReactDOM.unmountComponentAtNode(div)
})
