/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { css, jsx, Text, Box } from "./view"
import { mix } from "./styles"

const Component = ({ bem, theme, index = "fore" }) => {
  const color = theme.colors[index]
  const other = theme.colors[index === "fore" ? "back" : "fore"]
  return (
    <Box
      className={bem("swatch")}
      css={css`
        display: inline-block;
      `}
    >
      <Text
        className={bem("swatch-hex")}
        css={css`
          padding: 0.5rem 0;
          padding-right: 0.5rem;
          font-size: 0.75rem;
          vertical-align: top;
          line-height: 1rem;
          text-transform: uppercase;
          font-family: "Anonymous Pro", monospace;
        `}
      >
        <Box
          className={bem("swatch-color", index)}
          css={css`
            border: 1px solid ${mix(color, other, 0.5)};
            display: inline-block;
            background-color: ${color};
            width: 1rem;
            height: 1rem;
            margin: 0 0.5rem 0;
            vertical-align: top;
          `}
        />
        {color}
      </Text>
    </Box>
  )
}

export default Component
