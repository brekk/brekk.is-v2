/** @jsx jsx */
/* @jsxFrag React.Fragment */
import { withThemeAndData, Text, Box, css, jsx } from "./view"
// eslint-disable-next-line
import React from "react"
import { map, pipe, curry } from "ramda"
import { pure } from "recompose"
import { NavLink } from "react-navi"
// import { trace } from "xtrace"

import { mix, blackOrWhite, fore, back, stylify } from "./styles"
import { style as linkStyle } from "./Link"
import memo from "memoizee"
import { PACKAGES_AND_DESCRIPTIONS } from "./constants"
import { WrappedInstall as Install } from "./CodeInstall"
const { entries } = Object

const NavLinkWhichScrollsToTop = p => (
  <NavLink
    {...p}
    onClick={() => {
      window && window.scroll && window.scroll({ top: 0, left: 0 })
    }}
  >
    {p.children}
  </NavLink>
)

const rawStyles = {
  logo: p => `
    fill: ${fore(p)};
    width: 50%;
    max-width: 14rem;
  `,
  "package-description": `
  `,
  "package-code": `
    font-family: "Anonymous Pro", monospace;
  `,
  "package-code-wrapper": p => `
    font-weight: 700;
    background-color: ${mix(fore(p), back(p), 0.25)}};
    color: ${back(p)};
    clear: both;
    padding: 0.25rem 0.5rem;
    margin: 1.5rem auto 1rem;
    width: 80%;
    max-width: 20rem;
    overflow: hidden;
  `,
  "package-item": x => {
    const mixed = mix(back(x), fore(x), 0.25)
    return `
    transition: background-color 0.3s ease-out
              , color 0.3s ease-out;
    color: ${fore(x)};
    list-style: none;
    font-weight: bold;
    border-bottom: 1px solid ${mixed};
    box-shadow: 0;
    cursor: pointer;
    &:last-of-type {
      border-bottom: 0;
    }
    &:hover {
      background-color: ${fore(x)};
      color: ${back(x)};
      .Home__package-link {
        color: ${back(x)};
      }
      .Home__logo--oss {
        fill: ${back(x)};
      }
      .Home__package-code-wrapper {
        background-color: ${mixed};
        color: ${fore(x)};
      }
    }
    @media (min-width: 640px) {
      border-bottom: 1px dotted ${mixed};
      &:nth-of-type(even) {
        border-left: 1px dotted ${mixed};
      }
      &:nth-last-of-type(2) {
        border-bottom: 0;
      }
    }
    @media (min-width: 832px) {
      &:nth-of-type(even) {
        border-left: 0;
      }
      &:nth-of-type(3n+2) {
        border-right: 1px dotted ${mixed};
        border-left: 1px dotted ${mixed};
      }
      &:nth-last-of-type(3) {
        border-bottom: 0;
      }
      &:nth-last-of-type(2) {
        border-right: 1px dotted ${mixed};
        border-left: 1px dotted ${mixed};
        border-bottom: 0;
      }
      &:last-child {
        border-bottom: 0;
      }
    }
  `
  }
}
export const styles = stylify(rawStyles)
styles["package-link"] = linkStyle

const printWithBase = curry((a, b) => a + " " + b)
const fillify = curry((fill, selector) => `.${selector} { fill: ${fill}; }`)

export const logoStyleWithBase = curry(
  memo((withBase, p, x) =>
    pipe(
      () => {
        const F = fore(p)
        const B = back(p)
        const base = `
          display: block;
          clear: both;
          margin: 1rem auto;
          padding: 0;
          fill: ${F};
        `
        const hi = blackOrWhite(B) === `#000`
        const offBase = mix(F, B, 0.5)
        const print = printWithBase(withBase ? base : "")
        const fillian = fillify(offBase)
        const out = pipe(
          fillian,
          print
        )
        if (x === `brainwave`) {
         return out('back')
        }
        if (x === `f-utility`) {
          return out("none")
        }
        if (x === "phantomscript") {
          return out("skull")
        }
        if (x === `katsu-curry`) {
          return out("jp")
        }
        if (x === `snang`) {
          return out(hi ? `highlight` : `backing`)
        }
        if (x === 'half-baked') {
          return out('secondary')
        }
        if (x === 'torpor') {
          return out('secondary')
        }
        if (x === `gitparty`) {
          return `
            fill: ${offBase};
            @keyframes blink {
              0% {
                fill: ${B};
              }
              100% {
                fill: ${F};
              }
            }
            @keyframes reverseBlink {
              0% {
                fill: ${F};
              }
              100% {
                fill: ${B};
              }
            }
            .group-1 {
              animation: blink 0.6s linear infinite;
            }
            .group-3 {
              animation: reverseBlink 0.6s linear infinite;
            }
          `
        }
        if (x === `ljs2`) {
          return out("pilcrow")
        }
        if (x === `blem`) {
          return out("shadow")
        }
        if (x === 'breakpoints') {
          return out('lines')
        }
        if (x === `entrust`) {
          return out("trust")
        }
        if (x === `xtrace`) {
          return out("x")
        }
        if (x === `handrail`) {
          return out("fold")
        }
        return base
      },
      css
    )()
  )
)
export const logoStyle = logoStyleWithBase(true)
export const Component = ({
  bem,
  theme,
  packageManager,
  RoutedLink = NavLinkWhichScrollsToTop
}) =>
  pipe(
    entries,
    map(([x, { owner, component: OSSIcon, description, cli }]) => (
      <Box
        px={0}
        py={1}
        m={0}
        width={[1, 1 / 2, 1 / 3]}
        key={x}
        className={bem("package-item")}
        css={[
          styles["package-item"]({ theme }),
          !OSSIcon
            ? css`
                padding-bottom: 1rem;
              `
            : null
        ]}
      >
        <Text>
          <RoutedLink
            href={`/writing/package/${x}`}
            className={bem("package-link")}
            css={styles["package-link"]({ theme })}
          >
            <div className={bem("package-item-wrapper")}>
              {OSSIcon && (
                <OSSIcon
                  className={bem("logo", "oss")}
                  css={[styles.logo({ theme }), logoStyle({ theme }, x)]}
                />
              )}
              <Text>
                <strong>{owner ? `@${owner}/${x}` : x}</strong>
                <span
                  className={bem("package-description")}
                  css={css`
                    margin-left: 0.75rem;
                    font-weight: normal;
                  `}
                >
                  {description}
                </span>
              </Text>
            </div>
            <Install
              {...{
                owner,
                bem,
                css: styles["package-code-wrapper"]({ theme }),
                pkg: x,
                packageManager
              }}
            />
          </RoutedLink>
        </Text>
      </Box>
    ))
  )(PACKAGES_AND_DESCRIPTIONS)

const decorate = pipe(
  withThemeAndData,
  pure
)
export default decorate(Component)
