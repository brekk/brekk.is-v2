/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { pure } from "recompose"
import { pipe } from "ramda"
import { withThemeAndData, Flex, Box, jsx, css } from "./view"
import { ReactComponent as Logo } from "./logos/brekk-is.svg"
import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome"
import ControlPanel from "./ControlPanel"

import { fore } from "./styles"
import { style as linkStyle } from "./Link"
import "./icons"

const decorate = pipe(
  withThemeAndData,
  pure
)

const breakpoint = `48rem`

const Header = ({ bem, theme }) => (
  <Flex
    css={css`
      flex-direction: column;
      text-align: center;
      align-items: center;
      @media (min-width: ${breakpoint}) {
        flex-direction: row;
        align-items: center;
      }
    `}
  >
    <Box
      css={css({
        width: "100%"
      })}
    >
      <Logo
        css={css`
          fill: ${fore({ theme })};
          width: 70%;
          padding: 0 15%;
          text-align: center;
          @media (min-width: ${breakpoint}) {
            width: 100%;
            padding: 0;
            max-width: 42rem;
          }
        `}
      />
      <ControlPanel bem={bem} />
    </Box>
    <Box
      as="header"
      className={bem("header")}
      css={css`
        text-align: center;
        font-family: "Barlow Semi Condensed", sans-serif;
        @media (min-width: ${breakpoint}) {
          width: 60%;
          max-width: 40rem;
        }
      `}
    >
      <h1 className={bem("name")}>Brekk Bockrath</h1>
      <h2 className={bem("role")}>JavaScript Developer / Web Designer</h2>
      <a
        className={bem("link", "asset")}
        css={[
          linkStyle({ theme, noUnderline: true }),
          css`
            display: block;
            margin-bottom: 1rem;
          `
        ]}
        href="https://brekk.is/static/resume.pdf"
      >
        <Icon
          className={bem("icon", "resume")}
          icon="file"
          css={css`
            margin-right: 0.5rem;
          `}
        />
        Resum&eacute; / CV
      </a>
      <Box className={bem("menu")} css={css({ marginBottom: "1rem" })}>
        {[
          ["gitlab", ["fab", "gitlab"]],
          ["github", ["fab", "github"]],
          ["dribbble", ["fab", "dribbble"]],
          ["twitter", ["fab", "twitter"]],
          ["calendly", "calendar-times"],
          ["codepen", ["fab", "codepen"]]
        ].map(([domain, icon]) => (
          <Box key={domain} className={bem("link-item")} width={1} m={0} py={1}>
            <a
              href={`//${domain}.com/brekk`}
              className={bem("link")}
              css={linkStyle({ theme, noUnderline: true })}
            >
              {icon && (
                <Icon
                  icon={icon}
                  className={bem("icon", "domain")}
                  css={css({ marginRight: "0.5rem" })}
                />
              )}

              {domain.substr(0, 1).toUpperCase() + domain.substr(1)}
            </a>
          </Box>
        ))}
      </Box>
    </Box>
  </Flex>
)

export default decorate(Header)
