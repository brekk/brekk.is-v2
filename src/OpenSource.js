/** @jsx jsx */
/* @jsxFrag React.Fragment */
import { Flex, Box, Heading, css, jsx, withThemeAndData } from "./view"
import { pipe } from "ramda"
// eslint-disable-next-line
import React from "react"
import { pure } from "recompose"
import Packages from "./OpenSourcePackages"
import PackagePreference from "./PackagePreference"
import { NavLink } from "react-navi"

export const hoc = pipe(
  withThemeAndData,
  pure
)

const OSS = ({ bem, RoutedLink = NavLink }) => (
  <Box
    as="section"
    className={bem("open-source")}
    width={1}
    css={css`
      text-align: center;
      font-family: "Barlow Semi Condensed", sans-serif;
    `}
  >
    <Box width={1}>
      <Heading
        className={bem("packages-title")}
        css={css({ textTransform: "uppercase", margin: "1rem 0" })}
      >
        Open Source Modules
      </Heading>
    </Box>
    <PackagePreference {...{ bem }} />
    <Flex
      className={bem("packages")}
      css={css`
        padding-bottom: 0.5rem;
      `}
      mx={0}
      flexWrap="wrap"
      justifyContent="space-around"
    >
      <Packages bem={bem} />
    </Flex>
  </Box>
)

export default hoc(OSS)
