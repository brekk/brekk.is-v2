/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { jsx, withTheme, Heading } from "./view"
import Link from "./Link"

import { slugify } from "./utils"

const LinkableHeader = x => {
  const kids = slugify(x.children)
  return (
    <Heading id={kids} {...x}>
      <>
        <Link href={`#` + kids}>#</Link>
        {` `}
        {x.children}
      </>
    </Heading>
  )
}
export default withTheme(LinkableHeader)
