/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { pure } from "recompose"
import { withTheme, jsx, css, Box } from "./view"
import Swatch from "./Swatch"
import { pipe } from "ramda"
import { style as linkStyle } from "./Link"

import PreviousColorButton from "./buttons/PreviousColor"
import FlipColorsButton from "./buttons/FlipColors"
import NextColorButton from "./buttons/NextColor"

const decorate = pipe(
  withTheme,
  pure
)

const Footer = ({ bem, theme }) => (
  <footer
    className={bem("footer")}
    css={css`
      font-family: "Barlow Semi Condensed", sans-serif;
      text-align: center;
    `}
  >
    <Box
      width={1}
      as="aside"
      css={css({ fontSize: "0.75rem;" })}
      className={bem("colophon")}
    >
      Created with a{" "}
      <a
        css={linkStyle({ theme })}
        className={bem("link", "palette")}
        href="#control-panel"
      >
        random palette
      </a>{" "}
      named <strong>{theme.name.replace(`~`, `flipped-`)}</strong>, made in part
      with{" "}
      <a
        href="//npmjs.org/package/hello-color"
        css={linkStyle({ theme })}
        className={bem("link", "colophon")}
      >
        hello-color
      </a>
    </Box>
    <Box width={1}>
      <Swatch bem={bem} theme={theme} index="fore" />
      <FlipColorsButton
        override={css`
          margin-bottom: 0.25rem;
        `}
        bem={bem}
      />
      <Swatch bem={bem} theme={theme} index="back" />
    </Box>
    <Box width={1}>
      <PreviousColorButton
        bem={bem}
        override={css`
          margin: 0.35rem;
        `}
      />
      <NextColorButton
        bem={bem}
        override={css`
          margin: 0.35rem;
        `}
      />
    </Box>
  </footer>
)

export default decorate(Footer)
