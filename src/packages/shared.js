/** @jsx jsx */
/* @jsxFrag React.Fragment */
import React from "react" // eslint-disable-line no-unused-vars
import { map, pipe } from "ramda"
import { NavLink } from "react-navi"
import { Text, jsx, css, Box, Heading, Flex } from "../view"
import Link from "../Link"
import blem from "blem"
import { fore } from "../styles"
import ControlPanel from "../ControlPanel"
import "../icons"
import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome"
import { WrappedInstall as Install } from "../CodeInstall"
import { logoStyleWithBase } from "../OpenSourcePackages"
import PackagePreference from "../PackagePreference"
import { DataContext } from "../data"
import Sample from "../CodeSample"
import HomeButton from "../buttons/Home"
const { entries } = Object

const logoStyleWithoutBase = logoStyleWithBase(false)

export const FeatureItem = ({ bem, children: x }) => (
  <li
    className={bem("feature")}
    css={css({
      marginTop: `1rem`,
      marginBottom: `1rem`
    })}
  >
    {x}
  </li>
)

export const SeeElsewhere = ({ pkg, hubOrLab = false, owner = "brekk" }) => {
  const origin = hubOrLab ? "hub" : "lab"
  return (
    <Flex flexDirection="row" justifyContent="space-between">
      <Link href={`//npmjs.org/package/${pkg}`}>
        See on NPM{" "}
        <Icon
          css={css({ marginLeft: "0.25rem" })}
          icon="external-link-square-alt"
        />
      </Link>
      <Link href={`//git${origin}.com/${owner}/${pkg}`}>
        See on Git{origin}{" "}
        <Icon
          css={css({ marginLeft: "0.25rem" })}
          icon="external-link-square-alt"
        />
      </Link>
    </Flex>
  )
}

export const PackageHeader = ({
  Logo,
  logoOverride,
  theme,
  data,
  bem,
  name,
  RoutedLink = NavLink,
  hubOrLab,
  owner
}) => {
  const defaultLogoStyles = [
    css({
      fill: fore({ theme })
    }),
    logoStyleWithoutBase({ theme }, name)
  ]
  const logoStyles = logoOverride
    ? defaultLogoStyles.concat(logoOverride)
    : defaultLogoStyles
  return (
    <>
      <HomeButton bem={bem} />
      <ControlPanel bem={bem} {...data} />
      <Box
        as="header"
        py={2}
        className={bem("header")}
        css={css({
          textAlign: "center"
        })}
      >
        <SeeElsewhere pkg={name} hubOrLab={hubOrLab} owner={owner} />
        <Logo css={logoStyles} />
        <Heading fontSize={4}>{name}</Heading>
        <Install owner={owner} pkg={name} packageManager={data.packageManager} />
        <PackagePreference
          bem={bem}
          preferYarnOrNPM={data.preferYarnOrNPM}
          packageManager={data.packageManager}
        />
      </Box>
    </>
  )
}
export const Package = props => {
  const {
    name,
    bem = blem("Package"),
    theme,
    RoutedLink = NavLink,
    paragraphs,
    features,
    samples,
    Logo,
    logoOverride,
    hubOrLab = true,
    owner = "brekk"
  } = props
  return (
    <DataContext.Consumer>
      {data => (
        <Flex
          flexDirection="column"
          className={bem("", name)}
          p={[3, 2]}
          width={[1, 0.8]}
          css={css`
            font-family: "Barlow Semi Condensed";
            margin: 0 auto;
            max-width: 40rem;
            text-align: center;
          `}
        >
          <PackageHeader
            {...{
              RoutedLink,
              Logo,
              logoOverride,
              theme,
              data,
              bem,
              name,
              hubOrLab,
              owner
            }}
          />
          <Flex flexDirection="column" css={css({ textAlign: "left" })}>
            {paragraphs &&
              paragraphs.map((P, i) => <P key={i} {...props} {...data} />)}
            {features && (
              <>
                <Heading fontSize={3} textAlign="center" pt={4} pb={2}>
                  Features
                </Heading>
                <ul className={bem("features")}>
                  {map(
                    ({ title, list }) => (
                      <FeatureItem key={title} bem={bem}>
                        <Text fontWeight="bold">{title}</Text>
                        <ul>
                          {list.map((li, i) => (
                            <li key={i}>{li({ ...props, ...data })}</li>
                          ))}
                        </ul>
                      </FeatureItem>
                    ),
                    features
                  )}
                </ul>
              </>
            )}
          </Flex>
          {samples &&
            pipe(
              entries,
              map(([k, s]) => (
                <Sample key={k} title={s.title}>
                  {s.code}
                </Sample>
              ))
            )(samples)}
          <ControlPanel bem={bem} {...data} />
          <HomeButton bem={bem} />
        </Flex>
      )}
    </DataContext.Consumer>
  )
}
