/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import styled from '@emotion/styled'
import {trace} from 'xtrace'
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import { merge, curry, of, flip, last, concat, propOr, init, map, pipe } from "ramda"
import {renderBreakpoints, renderComponents, pointsToDirectionalAttributes} from '@open-sorcerers/breakpoints/breakpoints.js'
import {asRem, HORIZONTAL_BREAKPOINTS} from 'bodypaint'

import Link, {style as linkStyle} from '../Link'
import { blem, jsx, Text } from "../view"
import { ReactComponent as Logo } from "../logos/breakpoints.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "breakpoints"

const samples = {
  basic: {
    title: "Render horizontal breakpoints",
    code: `
import {
  asRem,
  HORIZONTAL_BREAKPOINTS
} from 'bodypaint'
import {
  renderBreakpoints
} from '@open-sorcerers/breakpoints'

export const Breakpoints = renderBreakpoints(
  asRem(16, HORIZONTAL_BREAKPOINTS)
)'
`
  },
  basicVertical: {
    title: "Render vertical breakpoints",
    code: `
import {
  asRem,
  VERTICAL_BREAKPOINTS
} from 'bodypaint'
import {
  renderVerticalBreakpoints
} from '@open-sorcerers/breakpoints'

export const VBreakpoints = renderVerticalBreakpoints(
  asRem(16, VERTICAL_BREAKPOINTS)
)'
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>
    <Code>breakpoints</Code> is a simple <Code>react</Code>-based tool for rendering breakpoints while developing websites. It has tooling which ostensibly would allow it to be used in a non-React context, but it was not necessarily designed as such.
  </Text>
)

const Paragraph2 = (props) => {
const {RoutedLink} = props
return (
  <Text py={2}>
    <Code>breakpoints</Code> pairs very well with <RoutedLink css={linkStyle(props)} href="/writing/package/bodypaint">bodypaint</RoutedLink>. For a working example of the two, please see <Link href="https://github.com/open-sorcerers/breakpoints-and-bodypaint-example">this example</Link>.
  </Text>
)}

const codify = (x) => <Code key={x}>{x}</Code>

const features = [
  {
    title: "renderBreakpoints",
    list: [
      () => <>A simple function for rendering ad-hoc horizontal breakpoints</>
    ]
  },
  {
    title: "renderVerticalBreakpoints",
    list: [
      () => <>A simple function for rendering ad-hoc vertical breakpoints</>
    ]
  },
  {
    title: "Breakpoint component",
    list: [
      () => codify("<Breakpoint/>")
    ]
  },
  {
    title: "VBreakpoint component",
    list: [
      () => codify("<VBreakpoint/>")
    ]
  },
]

const Breakpoint = props => {
  const Styled = styled.div(`
    position: fixed;
    height: 100vh;
    width: 1rem;
    z-index: 100;
    top: 0;
    left: ${propOr(0, 'left', props)};
    border-left: 1px dashed ${propOr('lime', 'fore', props)};
    opacity: ${propOr(0.1, 'opacity', props)};
    cursor: crosshair;
    &:hover {
      opacity: 1;
    }
    &::before {
      position: absolute;
      background-color: ${propOr('lime', 'fore', props)};
      color: ${propOr('black', 'back', props)};
      content: '${propOr('?', 'label', props)}';
      transform: rotate(-90deg);
      padding: 0 3rem 0 1rem;
      width: 10rem;
      margin-left: -7rem;
      margin-top: 2rem;
    }
  `)
  return <Styled />
}

const CustomBreakpoints = ({theme})=> {
  const {colors} = theme
  const Points = pipe(
    asRem(16),
    pointsToDirectionalAttributes('left'),
    map(merge(colors)),
    renderComponents(Breakpoint)
  )(HORIZONTAL_BREAKPOINTS)
  return <Points />
}

const Breakpoints = renderBreakpoints(asRem(16, HORIZONTAL_BREAKPOINTS))

const Component = ({ bem, name, theme, RoutedLink }) => {
  return (
  <>
    <CustomBreakpoints theme={theme}/> 
    <Package
      name={name}
      bem={bem}
      theme={theme}
      RoutedLink={RoutedLink}
      paragraphs={[Paragraph1, Paragraph2]}
      features={features}
      samples={samples}
      Logo={Logo}
      owner="open-sorcerers"
    />
  </>
)
}
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
