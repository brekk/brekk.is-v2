/** @jsx jsx */
/* @jsxFrag React.Fragment */
import React from "react" // eslint-disable-line no-unused-vars
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import { join, map, pipe } from 'ramda'
import { blem, jsx, Text } from "../view"
import Code from "../Code"

import { ReactComponent as Logo } from "../logos/snang.svg"
import "../icons"

import { Package } from "./shared"

export const name = "snang"



const Paragraph1 = () => (
  <Text py={2}>
    Use inline javascript to manipulate text streams. Based on ideas from{" "}
    <Code>jayin</Code> and <Code>pipeable-js</Code>
  </Text>
)
const Paragraph2 = () => (
  <Text py={2}>
    <ol>
      <li>
        <Code>x</Code> refers to the current buffer contents
      </li>
      <li>
        Both <Code>ramda</Code> and <Code>entrust</Code> have been injected into
        the sandbox, so any method of theirs will work
      </li>
      <li>
        <Code>fluture</Code> has also been injected into the sandbox, but it is aliased to the literal <Code>F</Code>.
      </li>
      <li>
        <Code>exec</Code> allows you to run <Code>child_process.execSync</Code>{" "}
        on the given string. This allows you to construct meta commands.
      </li>
    </ol>
  </Text>
)

const config = [ 
[[`debug`, `d`], [`Pass integers in to get specific debug information`]],
[[`help`, `h`], [`See this help text`]],
[[`color`, `C`], [`Display output with color. Use --no-color to turn color off.`]],
[[`shell`, `P`], [`Specify pipe commands with "|" delimiters, like in *nix`]],
[[`pipe`, `p`], [`Wrap passed expression in pipe`]],
[[`compose`, `c`], [`Wrap passed expression in compose`]],
[[`json`, `i`], [`Read JSON values in`]],
[[`jsonOut`, `o`], [`Pass JSON values out`]],
[[`prettyPrint`, `L`], [`Print the commands you've passed into snang, but pretty`]],
[[`print`, `l`], [`Print the commands you've passed into snang`]],
[[`exportFile`, `x`], [`Print the commands you've passed into snang, but as a file`]],
[[`exportES6`, `X`], [`Print the commands you've passed into snang, but as an ES6 file`]],
[[`readStdinOnExport`, `t`], [`Used in concert with -X / -x, this makes the resulting file deal with stdin as an input`]],
[[`readFileOnExport`, `u`], [`Used in concert with -X / -x, this makes the resulting file deal with a file as an input`]],
[[`readDirOnExport`, `v`], [`Used in concert with -X / -x, this makes the resulting file deal with a directory as an input`]],
[[`read`, `r`], [`Read from a file. If this is not passed, snang reads from stdin.`]],
[[`write`, `w`], [`Write from a file. If this is not passed, snang writes to stdout.`]],
[[`require`, `q`], [`Add a commonjs file to be used within snang's vm.`]],
[[`import`, `Q`], [`Add an ES6 style file to be used within snang's vm. Impacts performance, as this transpiles sources on the fly.`]],
[[`future`, `f`], [`If the resulting output of the expression is a Future, fork it to (stderr, stdout).`]],
[[`trim`, `m`], [`Trim the trailing \\n of the input. Default: true`]],
[[`source`, `s`], [`Add a source file which takes the form --source ref:path/to/file.js. Adds a source object to the VM which has sources as Futures.`]],
[[`config`, `k`], [`Pass a config file to snang. Uses cosmiconfig, so any of the following is valid: '.snangrc' '.snangrc.json' '.snangrc.js' '.snangrc.yaml' '.snangrc.config.js' or a "snang" property in package.json.`]]
]

const enflag = x => <Code>{(x.length == 1 ? '-' + x : '--' + x)}</Code>

const ParagraphFlags = () => (
<>
  <h2>Flags</h2>
  <dl>{(
    pipe(
     map(([flags, def]) => (
       <>
       <dt>
         {pipe(map(enflag), ([long, short]) => <>{long}, {short}</>)(flags)}
       </dt>
       <dd>{def}</dd>
       </>
     ))
    )(config)
  )}
  </dl>
</>
)
const samples = {
  grabStuffFromPackageJson: {
    title: "Grab eslint related devDependencies",
    code: `
cat package.json | snang -iP "prop('devDependencies') | keys | filter(includes('eslint')) | join(C._)" 
`
  },
  readNameFourWays: {
    title: `read name from package.json`,
    code: `
// each line below is equivalent
snang --read package.json --json "x.name"                               
snang --read package.json --json --pipe "prop('name')"
cat package.json | snang -i "x.name"
cat package.json | snang -ip "prop('name')"
`
  },
 listEslintDeps: {
    title: "list the devDependencies in package.json which have eslint in the name of the key, as a string",
    code: `
// each line below is equivalent
snang --read package.json --json --shell "prop('devDependencies') | keys | filter(matches('eslint')) | join(' ')"
cat package.json | snang -iP "prop('devDependencies') | keys | filter(matches('eslint')) | join(' ')"
`
 },
 exec: {
   title: `read package.json, filter devDependencies and then pass to yarn and execute`,
   code: `
// each line below is equivalent
cat package.json | snang --json "exec( 'yarn add ' + Object.keys(x.devDependencies).filter(z => z.includes('eslint')).join(' ') + '\-D' )"
cat package.json | snang -iP "prop('devDependencies') | keys | filter(includes('eslint')) | join(' ') | z => 'yarn add ' + z + ' -D') | exec"
cat package.json | snang -iP "prop('devDependencies') | keys | filter(includes('eslint')) | join(' ')" | xargs yarn add -D
`},
 readLocalModule: {
   title: `read package.json, require local node-module (with optional alias)`,
   code: `
// each line below is equivalent
snang --read package.json --json --require camel-case -P "prop('devDependencies') | keys | map(camelCase.camelCase)" -o
snang --read package.json --json --require kool:camel-case -P "prop('devDependencies') | keys | map(kool.camelCase))" -o
`
 },
 importLocalModule: {
   title: `read package.json, import es6 module (with optional alias)`,
   code: `
// each line below is equivalent
snang --read package.json --json --import ./require-test-simple.mjs -P "prop('devDependencies') | keys | map(requireTestSimple)" -o
snang --read package.json --json --import kool:./require-test-simple.mjs -P "prop('devDependencies') | keys | map(kool))" -o
`
 }
}

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2, ParagraphFlags]}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
