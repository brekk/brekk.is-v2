/** @jsx jsx */
/* @jsxFrag React.Fragment */
import React from "react" // eslint-disable-line no-unused-vars
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import Link from "../Link"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/blem.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "blem"

const samples = {
  basic: {
    title: "Generate a BEM-style selector",
    code: `
import blem from 'blem'
 
const bem = blem('xxx')
 
// block
bem() === 'xxx'
// block with modifier
bem('', 'cool') === 'xxx xxx--cool'
// block with modifiers
bem('', 'abc'.split('')) === 'xxx xxx--a xxx-b xxx-c'
 
// element
bem('yyy') === 'xxx__yyy'
// element with modifier
bem('yyy', 'zzz') === 'xxx__yyy xxx__yyy--zzz'
// element with modifiers
bem('yyy', 'abc'.split('')) === 'xxx__yyy xxx__yyy--a xxx__yyy--b xxx__yyy--c'
 
/*
const X = ({
  bem = blem('xxx')
  title
}) => (
  <div className={bem()}>
    <strong className={bem('title')}>
      {title}
    </strong>
  </div>
)
*/
`
  }
}

const features = [
  {
    title: "Memoized BEM string generators",
    list: [() => <Code>blem</Code>]
  }
]

const Paragraph1 = () => (
  <Text py={2}>
    blem allows for the creation of BEM notation strings (originally{" "}
    <Link href="https://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/">
      written about here
    </Link>
    ) in a simple and intuitive manner.
  </Text>
)

const Paragraph2 = () => (
  <Text py={2}>
    This portfolio site uses blem extensively. Use the developer tools to
    inspect and see the architecture.
  </Text>
)

const Paragraph3 = () => (
  <Text py={2}>
    For a simple code sandbox in which to play with blem,{" "}
    <Link href="https://codepen.io/brekk/pen/WLzEqo">
      see this example on codepen
    </Link>
    .
  </Text>
)

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2, Paragraph3]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
