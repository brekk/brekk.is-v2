/** @jsx jsx */
/* @jsxFrag React.Fragment */
import React from "react" // eslint-disable-line no-unused-vars
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import Link from "../Link"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/half-baked.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "half-baked"

const samples = {
  bezierk: {
    title: "Local file-based static server for the lazy",
    code: `
const { fork, chain } = require('fluture')
const { writeFile } = require('torpor')
const { merge, pipe, __ } = require('ramda')

const halfBakery = require('./half-baked')

const jj = (nn) => (xx) => JSON.stringify(xx, null, nn)
const j0 = jj(0)
// const j2 = jj(2)

const slugify = (x) => x.toLowerCase().replace(/\W/g, '-')

const CONFIG = {
  PORT: 3001,
  STORAGE: {
    BRAIN: 'bezierk.json',
    BACKUP: 'bezierk.json.bak'
  },
  logging: true,
  onPostRoot: ({ updateBrain }) => (req, res, next) => {
    const { points, name } = req.body
    const file = slugify(name) + ".json"
    const update = (raw) =>
      merge(raw, { files: (raw.files || []).concat(file) })
    pipe(
      j0,
      writeFile("./" + file", __, 'utf8'),
      chain(() => updateBrain(update)),
      fork(next)(() => res.json({ saved: true }))
    )({ points, meta: { modified: new Date().toString() } })
  }
}

fork(console.warn)(({ config }) => {
  const storage = config.STORAGE.BRAIN
  const accessPath = config.STORAGE.ACCESS_PATH
  const accessString = accessPath.slice(0, -1).join('.') + '[id]'
  const host = "http://localhost:" + config.PORT
  console.log(\`
BEZIERK! local server
======== $\{host} =======================================
    HEAD $\{host}/    ~ 204
    GET  $\{host}/    ~ 200 + $\{storage}
    POST $\{host}/    ~ {points, meta: {modified}}
    HEAD $\{host}/:id ~ 204
    GET  $\{host}/:id ~ 200 + $\{storage}.$\{accessString}
\`)
})(halfBakery(CONFIG))
`
  }
}

const features = [
  {
    title: "Read from a structured local JSON file",
    list: [() => <Code>STORAGE.BRAIN</Code>]
  },
  {
    title: "Skip debugging local CORS errors",
    list: [() => <Code>Response: HEAD 204</Code>]
  },
  {
    title: "Based on express, so it's very easy to replace",
    list: []
  }
]

const Paragraph1 = () => (
  <Text py={2}>
    half-baked is an opinionated, stupid simple library designed for local development. It uses <code>express</code> under the hood, so it is easy to replace, but has a number of easy to use affordances.
  </Text>
)

const expectedShape = `{ "data": [{"id":1, "name": "croissant"}, {"id": 2, "name": "eclair"}] }`
const expectedCroissant = `{"id": 1, "name": "croissant"}`

const Paragraph2 = () => (
  <Text py={2}>
    half-baked expects a local JSON file (by default: <code>half-bakedata.json</code>), which is structured like so: <Code>{expectedShape}</Code>. From there, it makes the entirety of the "data" key available at the root path (e.g. <code>"localhost:3001"</code>). For any valid <code>"id"</code> in the <code>"data"</code>, it will serve that data at the path <code>"/:id"</code>, (e.g. <code>"localhost:3001/1"</code> would return the <code>{expectedCroissant}</code> object). It also responds with a 204 value for any <code>HEAD</code> request, so local CORS errors are a thing of the past™ . Finally, it has simple hooks in place for customizing your local server simply, with an escape hatch for writing against the raw <code>express</code> instance it uses under the hood.
  </Text>
)

const Paragraph3 = () => (
  <Text py={2}>
    <h2>Hooks</h2>
    <dl>
      <>
        <dt><code>onCancel</code></dt>
        <dd>what to do when things go awry</dd>
      </>
      <>
        <dt><code>onGetId</code></dt>
        <dd>what to do when serving an id path: <code>(`/:id`)</code></dd>
      </>
      <>
        <dt><code>onGetRoot</code></dt>
        <dd>what to do when serving the root path <code>(`/`)</code></dd>
      </>
      <>
        <dt><code>onPostId</code></dt>
        <dd>what to do when posting data to an id path <code>(`/:id`)</code></dd>
      </>
      <>
        <dt><code>onPostRoot</code></dt>
        <dd>what to do when posting data to the root path <code>(`/`)</code></dd>
      </>
    </dl>
  </Text>
)

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2, Paragraph3]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
