/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { pure } from "recompose"
import { withTheme } from "emotion-theming"
import Link from "../Link"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/katsu-curry.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

const samples = {
  basic: {
    title: "Deal with parameters at your leisure",
    code: `
import {curry} from 'katsu-curry'
const add = curry((a, b, c) => a + b + c)
add(1,2,3)
add(1,2)(3)
add(1)(2)(3)
`
  },
  placeholder: {
    title: "Use the placeholder to skip parameters",
    code: `
import {curry, $} from 'katsu-curry'
// $ is also aliased to PLACEHOLDER
const divide = curry((a,b) => a / b)
divide(2) // of limited utility
divide(2)(3) // 2/3
divide($,2)(3) // 3/2
`
  },
  debugMode: {
    title: "Use debug mode for additional clarity",
    code: `
import {curry} from 'katsu-curry/debug'
const over = (a,b) => a / b // named functions are useful!
const divide = curry(over)
divide(2).toString() // curry(over)(2)(?)
divide(2)(3) // 2/3
divide($,2).toString() // curry(over)(🍛,2)(?)
`
  },
  objectModeByArity: {
    title: "Use object-mode with a specific arity",
    code: `
import {curryObjectN} from 'katsu-curry'
const threeKeyProps = curryObjectN(3, Object.keys)
threeKeyProps({a: 1, b: 2, c: 3}) // ['a', 'b', 'c']
const oneMore = threeKeyProps({a: 1, b: 2}) // function expecting one more param
const encase = (x) => ({[x]: x})
'cdefghi'.split('').map(encase).map(oneMore)
/*
[ [ 'a', 'b', 'c' ],
  [ 'a', 'b', 'd' ],
  [ 'a', 'b', 'e' ],
  [ 'a', 'b', 'f' ],
  [ 'a', 'b', 'g' ],
  [ 'a', 'b', 'h' ],
  [ 'a', 'b', 'i' ] ]
*/
`
  },
  objectModeByKeys: {
    title: "Use object-mode with specific keys",
    code: `
import {curryObjectK} from 'katsu-curry'
const abc = curryObjectK(['a', 'b', 'c'], Object.values)
abc({a: 1, b: 2, c: 3}) // [1, 2, 3]
const oneMore = abc({a: 1, b: 2}) // function expecting "c"
const encase = (x) => ({[x]: x})
'cdefghi'.split('').map(encase).map(oneMore)
/*
[ [ 1, 2, 3 ],
  function,
  function,
  function,
  function,
  function,
  function ]
*/    
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>
    <Link href="//fr.umio.us/favoring-curry/">Currying</Link> is a technique for
    taking a function which expects multiple parameters into one which, when
    supplied with{" "}
    <Link href="//brekk.github.io/katsu-curry/docs/API.html#curry">
      fewer parameters
    </Link>{" "}
    (or{" "}
    <Link href="//brekk.github.io/katsu-curry/docs/API.html#curryObjectN">
      other expectations
    </Link>
    ) returns a new function that awaits the remaining ones.
  </Text>
)

const Paragraph2 = () => (
  <Text py={2}>
    This library provides solutions for{" "}
    <Link href="//brekk.github.io/katsu-curry/docs/API.html#curry">
      traditional curry
    </Link>
    , specifying a{" "}
    <Link href="//brekk.github.io/katsu-curry/docs/API.html#curryObjectK">
      number of keys for object-style curry
    </Link>{" "}
    (as originated by the <Link href="//npmjs.org/package/fpo">fpo</Link> module
    and <Link href="//leanpub.com/fljs">this book</Link>) as well as a
    (currently underwhelming in performance){" "}
    <Link href="//brekk.github.io/katsu-curry/docs/API.html#curryObjectK">
      explicit-key object-style curry
    </Link>
    .
  </Text>
)

const Paragraph3 = () => (
  <Text py={2}>
    Additionally, similarly to{" "}
    <Link href="//brekk.github.io/katsu-curry/#benchmark">other libraries</Link>{" "}
    in the space, this library provides a "placeholder" value which can be used
    to{" "}
    <Link href="//brekk.github.io/katsu-curry/docs/API-in-debug-mode.html">
      omit a parameter
    </Link>{" "}
    in partial-application.
  </Text>
)

const features = [
  {
    title: "Traditional parameter currying:",
    list: [() => <Code>curry</Code>]
  },
  {
    title: "Object-style currying:",
    list: [
      () => <Code>curryObjectK</Code>,
      () => <Code>curryObjectN</Code>,
      () => <Code>curryObjectKN</Code>
    ]
  },
  {
    title: "Partial application with any combination of arguments",
    list: [
      () => (
        <>
          <Code>$</Code> or <Code>PLACEHOLDER</Code>
        </>
      )
    ]
  },
  {
    title: "Utilities",
    list: [
      () => (
        <>
          for remapping parameters: <Code>remap</Code> and{" "}
          <Code>remapArray</Code>
        </>
      ),
      () => (
        <>
          for function composition: <Code>pipe</Code> and <Code>compose</Code>
        </>
      ),
      () => (
        <>
          for function constants: <Code>K</Code>
        </>
      ),
      () => (
        <>
          for functional identity: <Code>I</Code>
        </>
      )
    ]
  },
  {
    title: "An outrageously useful debug-mode™",
    list: [
      () => `add clarity when things aren't working (at the cost of speed)`,

      () => (
        <>
          <Code>import debug from 'katsu-curry/debug'</Code>
        </>
      ),
      () => (
        <>
          or <Code>{`require('katsu-curry/debug')`}</Code>
        </>
      )
    ]
  }
]

const KatsuCurry = ({
  name = "katsu-curry",
  bem = blem("Package"),
  theme,
  RoutedLink = NavLink
}) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2, Paragraph3]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withTheme,
  pure
)

export default decorator(KatsuCurry)
