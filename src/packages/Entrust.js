/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/entrust.svg"
import "../icons"

import Sample from "../CodeSample"
import Code from "../Code"
import { Package } from "./shared"
import { style as linkStyle } from "../Link"

export const name = "entrust"

const samples = {
  basic: {
    title: "Common uses",
    code: `
import {e0, e1, e2} from 'entrust'
const toUpper = e0('toUpperCase')
// toUpper('xyz') === 'XYZ'
const toLower = e0('toLowerCase')
// toLower('XYZ') === 'xyz'
const split = e1('split')
// split('x', 'axbxc') === ['a', 'b', 'c']
const join = e1('join')
// join('x', ['a', 'b', 'c']) === 'axbxc'
const map = e1('map')
// map(x => x * 2, [1,2,3]) === [2,4,6]
const reduce = e2('reduce')
// reduce(
//   (a, b) => a + b,
//   0,
//   [1,2,3,4,5]
// ) === 15
`
  },
  withComposition: {
    title: "Using entrust with composition",
    code: `
import {pipe} from 'ramda' // or lodash/fp
const talkProper = pipe(
  e1('split')(' '),
  e1('map')(
    x => x[0].toUpperCase() + x.substr(1)
  ),
  e1('join')(' ')
)
// talkProper('entrust is cool') === 'Entrust Is Cool'
`
  }
}

const Paragraph1 = () => (
  <>
    <Text py={2}>
      You can use <Code>entrust</Code> to easily transform methods into a
      curried, delegatee-last (data-last) form. If you are used to functional
      programming (FP) paradigms, this makes FP in JS much easier.
    </Text>
    <Sample title="Example" textAlign="center">
      {`
import {e0} from 'entrust'
e0('toUpperCase')('abcde') === 'ABCDE'
`}
    </Sample>
  </>
)
const Paragraph2 = props => {
  const { RoutedLink } = props
  return (
    <Text py={2}>
      <Code>entrust</Code> is built using{" "}
      <RoutedLink css={linkStyle(props)} href="/writing/package/katsu-curry">
        katsu-curry
      </RoutedLink>
      's <Code>curry</Code> implementation. However, if you would like to use a
      different curry implementation, this is easily accomplished using the{" "}
      <Code>custom</Code> API. This technique is used to generate entrust's
      "debug-mode" export.
    </Text>
  )
}
const Paragraph3 = props => {
  const { RoutedLink } = props
  return (
    <Text py={2}>
      <em>
        NB: This approach is used extensively in the{" "}
        <RoutedLink css={linkStyle(props)} href="/writing/package/f-utility">
          f-utility
        </RoutedLink>{" "}
        module.
      </em>
    </Text>
  )
}

const features = [
  {
    title: "Delegatee-last curried function generators",
    list: [`e0`, `e1`, `...`, `e9`, `e10`].map(x => () =>
      ~x.indexOf(".") ? x : <Code>{x}</Code>
    )
  },
  {
    title: "Invoke delegated methods with array-based arguments",
    list: [() => <Code>eN</Code>]
  },
  {
    title: "Outrageously useful debug-mode™",
    list: [() => <Code>eD</Code>]
  }
]

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2, Paragraph3]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
