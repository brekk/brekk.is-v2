/** @jsx jsx */
/* @jsxFrag React.Fragment */
import React from "react" // eslint-disable-line no-unused-vars
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/ljs2.svg"
import "../icons"

import { Package } from "./shared"

export const name = "ljs2"

const samples = {
  includeMD: {
    title: "an-includable-file.md",
    code: `
This is an *import*!
`
  },
  includeJS: {
    title: "an-includable-literate-file.js",
    code: `
const otherCode = () => {}
`
  },
  jsfile: {
    title: "a-literate-js-file.js",
    code: `
// # a-literate-js-file.js
// # use the '#' character to ignore a line on a single line
/**
The slash-double-asterisk above is called a "magic header" and this makes this block comment visible in the resulting markdown file.
# h1
_markdown_!
*/
// # use the "=>" magic directive to either:
// # read some other file as a plain document
// => plain some-file.md
// # or read some other file as another literate JS file
// => include some-include.js

const myCode = () => {
  // ostensibly you'd put some real code here that does something
}
`
  },
  command: {
    title: "Run ljs2!",
    code: `
$ npx ljs2 ./my-example-file.js
`
  },
  result: {
    title: "Results",
    code: `
The slash-double-asterisk above is called a "magic header" and this makes this block comment visible in the resulting markdown file.
# h1
_markdown_!

This is an *import*!

\`\`\`js
const otherCode = () => {}
\`\`\`

\`\`\`js
const myCode = () => {
  // ostensibly you'd put some real code here that does something
}
\`\`\`
    `
  }
}

const Paragraph1 = () => (
  <Text py={2}>LJS2 is a tool for literate programming.</Text>
)

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1]}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
