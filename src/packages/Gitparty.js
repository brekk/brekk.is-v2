/** @jsx jsx */
/* @jsxFrag React.Fragment */
import React from "react" // eslint-disable-line no-unused-vars
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/gitparty.svg"
import "../icons"

import { Package } from "./shared"

export const name = "gitparty"

const samples = {
  an_example_gitpartyrc: {
    title: ".gitpartyrc",
    code: `
styles:
  key: S
  color: magenta
  matches:
  - \**/*.less
tests:
  key: T
  color: bgGreen
  matches:
  - \**/*.robot
  - Jenkinsfile
  - \**/*.gradle
backend:
  key: B
  color: bgBlue
  matches:
  - \**/*.java
  - \**/*.soy
  - \**/*.html
  - \**/*.py
  - \**/*.bat
  - \**/*.jar
  - \**/*.properties
frontend:
  key: F
  color: bgRed
  matches:
  - \**/*.js
  - \**/*.jsx
config:
  key: C
  color: bgCyan
  matches:
  - \**/.gitignore
  - \**/package.json
  - \**/*.json
  - \**/^.*
  - \**/.eslintrc
  - README.md
dependencies:
  key: D
  color: bgYellow
  matches:
  - \**/package.json
  - \**/yarn.lock
hope:
  key: H
  color: bgBrightRed
  filter:
  - subject:should~
  - subject:fix~
  - subject:work~
timezone: Europe/Berlin
collapseAuthors: true 
aliases:
  💡brekk:
  - brekk
  - bbockrath 
  💡John:
  - John Smith
  - jsmith
  team-commercial:
  - Than Nguyen
  team-advertising:
  - Adam Vertising
  - Bhavesh Kindamar
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>
    Add more visibility and specificity than a standard git log view
  </Text>
)

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1]}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
