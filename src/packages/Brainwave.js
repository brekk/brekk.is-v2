/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import styled from '@emotion/styled'
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"

import CodeSample from '../CodeSample'
import Link, {style as linkStyle} from '../Link'
import { blem, jsx, Text } from "../view"

import { pathOr, curry, of, flip, last, concat, init, map, pipe } from "ramda"
import { ReactComponent as Logo } from "../logos/brainwave.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "brainwave"

const samples = {
  
}

const Paragraph1 = () => (
  <Text py={2}>
    Use <Code>brainwave</Code> to walk your local files and control the ones with frontmatter in them.
    <br />
    <br />
    Let's say you have a bunch of files with frontmatter in them. Like MDX or similar. Let's say you have fields in that frontmatter that you wanna be able to query against, or manipulate, but you don't wanna do it by hand.
    <br />
    <br />
    Use <Code>brainwave</Code>!
  </Text>
)
const Paragraph2 = () => (
  <>
  <h3>Disclaimer</h3>
  <p>
    <Code>brainwave</Code> is a powerful tool which can change tons of files in your codebase. Using <Code>brainwave</Code> in a folder without version control is likely a mistake. <strong>Protect yourself.</strong> Remember to use the `--dryRun` flag to see what would have run instead. Use brainwave at your own risk. Please log any <Link href="https://github.com/open-sorcerers/open-sorcerers/issues">issues</Link> on Github.
  </p>
  </>
)

const Paragraph3 = () => (
<>
  <h2>Usage</h2>
  <ol>
    <li><Code>cd your-project</Code></li>
    <li><Code>brainwave --init</Code> - generate a basic <Code>brainwave.config.js</Code> file</li>
    <li>Open <Code>brainwave.config.js</Code> in your favorite editor and make changes relative to your codebase:</li>
  </ol>
  <CodeSample title="brainwave.config.js">{(`module.exports = {
  // telepathy specifies function predicates to run against the codebase
  telepathy: {
    // this will print out all files which have a "review" key in their frontmatter
    review: z => z && z.review
  },
  // control specifies both predicates and mutations, each a function
  control: {
    edited: [
      // *REMEMBER* to use --dryRun and it will instead tell you what _would_ be modified
      // this runs always!
      () => true,
      // it will add / update dateEdited field in the mdx files
      x => (
        x && x.stats && x.stats.ctime
        ? { dateEdited: new Date(x.stats.ctime) }
        : {}
      )
    ]
  }
}`)}</CodeSample>
</>
)

const features = [
  {
    title: "telepathy",
    list: [
      () => <>Specify function predicates to run against the codebase</>
    ]
  },
  {
    title: "control",
    list: [
      () => <>Specify predicates and mutations, each a function, to run against the codebase.</>
    ]
  }
]

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2, Paragraph3]}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
