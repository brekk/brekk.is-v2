/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import Link from '../Link'
import { blem, jsx, Text } from "../view"
import { curry, of, flip, last, concat, init, map, pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/torpor.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "torpor"

const samples = {
  basic: {
    title: "Example usage",
    code: `
const path = require('path')
const {
  __,
  chain,
  head,
  map,
  pipe,
  prop,
  slice
} = require('ramda')
const { readFile } = require('torpor')
const { futurizeWithCancel } = require('ensorcel')
const { fork } = require('fluture')
// const { trace } = require('xtrace')
const SVGO = require('svgo')
const fast = require('fast-xml-parser')

const sviggie = new SVGO()
const optimize = futurizeWithCancel(
  () => {},
  1,
  (x) => sviggie.optimize(x)
)
const relative = (x) => path.resolve(process.cwd(), x)

const j2 = (x) => JSON.stringify(x, null, 2)

const toJSON = pipe(
  slice(2, Infinity),
  head,
  relative,
  readFile(__, 'utf8'),
  chain(optimize),
  map(
    pipe(
      prop('data'),
      (raw) =>
        fast.parse(raw, {
          ignoreAttributes: false,
          parseAttributeValue: true,
          attributeNamePrefix: ''
        }),
      j2
    )
  ),
  fork(console.warn)(console.log)
)

toJSON(process.argv)
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>
    <Code>torpor</Code> is a <Link href="//github.com/fluture-js/Fluture">Future-based wrapper</Link> around the standard <Code>fs</Code> (filesystem) module. It allows for a simple and convenient, lazy, asynchronous interface for dealing with the filesystem.
  </Text>
)

const fsMethods = [
  'access',
  'appendFile',
  'chmod',
  'chown',
  'close',
  'constants',
  'copyFile',
  'createReadStream',
  'createWriteStream',
  'fchmod',
  'fchown',
  'fdatasync',
  'fstat',
  'fsync',
  'ftruncate',
  'futimes',
  'lchmod',
  'lchown',
  'link',
  'lstat',
  'mkdir',
  'mkdtemp',
  'open',
  'read',
  'readFile',
  'readdir',
  'readlink',
  'realpath',
  'rename',
  'rmdir',
  'stat',
  'symlink',
  'truncate',
  'unlink',
  'utimes',
  'write',
  'writeFile'
]

const codify = curry(
  (close, fn) => <><Code key={fn}>{fn}</Code>{close ? ', ' : ''}</>
)

const features = [
  {
    title: "Magical Future-based filesystem interface",
    list: [() => <p>
      {
        pipe(
          init,
          map(codify(true)),
          flip(concat)(pipe(last, codify(false), of)(fsMethods))
        )(fsMethods)
      }
    </p>]
  }
]

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
