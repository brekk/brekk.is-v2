/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import Link, {style as linkStyle} from '../Link'
import { blem, jsx, Text } from "../view"
import { curry, of, flip, last, concat, init, map, pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/bodypaint.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "bodypaint"

const samples = {
  basic: {
    title: "Set styles via named breakpoints",
    code: `
/** @jsxImportSource @emotion/react */
import { bodypaint } from 'bodypaint'
import { css } from '@emotion/react'

const POINTS = {
  TINY: 1,
  SMALL: 320,
  MEDIUM: 640,
  LARGE: 960
}

const mq = makePainter({
  useMin: true,
  useHeight: false,
  baseFontSize: 16,
  implicit: true
  points: POINTS
})

const responsiveStyles = mq({
  background: {
    TINY: 'lime',
    SMALL: 'yellow',
    MEDIUM: 'orange',
    LARGE: 'red'
  }
})

const styles = css\`
   width: 2rem;
   height: 2rem;
   \${responsiveStyles}
\`

const Box = () => <div css={styles}>This is a box.</div>
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>
    <Code>bodypaint</Code> is a library for improving the <Code>emotion</Code> API for responsive design. It is similar in nature to <Link href="https://www.npmjs.com/package/facepaint">facepaint</Link> (and relies upon it internally) but has a more natural API.
  </Text>
)

const Paragraph2 = (props) => {
  const {RoutedLink} = props
  return (
    <Text py={2}>
      <Code>bodypaint</Code> pairs very well with <RoutedLink css={linkStyle(props)} href="/writing/package/breakpoints">breakpoints</RoutedLink>. For a working example of the two, please see <Link href="https://github.com/open-sorcerers/breakpoints-and-bodypaint-example">this example</Link>.
    </Text>
  )
}

const codify = (x) => <Code key={x}>{x}</Code>

const apiMethods = [
  `makePainter`,
  `bodypaint`
]

const features = [
  {
    title: "Default export",
    list: [
      () => codify("bodypaint")
    ]
  },
  { title: "Configurable export",
    list: [
    () => codify("makePainter")
    ]
  },
  {
    title: "API",
    list: [
      () => (<><strong>useMin</strong> &mdash; use minimum or maximum?</>),
      () => (<><strong>useHeight</strong> &mdash; use width or height?</>),
      () => (<><strong>baseFontSize</strong> &mdash; font size in pixels</>),
      () => (<><strong>implicit</strong> &mdash; fill any implicit gaps</>),
      () => (<><strong>points</strong> &mdash; a list or named object of points</>),
    ]
  }
]

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
