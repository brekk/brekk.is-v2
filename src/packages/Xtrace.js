/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/xtrace.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "xtrace"

const samples = {
  idiomatic: {
    title: "Add painless logging",
    code: `
import { trace } from "xtrace"
import { pipe } from "ramda"
const myFunction = pipe(
  functionA,
  trace("from A -> B"),
  functionB
)
`
  },
  basic: {
    title: "Add transparent side-effects",
    code: `
import { callWithScopeWhen } from "xtrace"
import { pipe, prop } from "ramda"
const logWhen = callWithScopeWhen(console.log)
const getId = prop('id')
const idAbove = above => id => id > above
// ... log as you calculate
const myFunction = pipe(
  logWhen(idAbove(300), getId, 'above three hundred'),
  calculate
)
`
  },
  taggedSideEffect: {
    title: "Add binary side-effects",
    code: `
import { callBinaryWithScopeWhen } from "xtrace"
import { prop, pipe } from "ramda"
const logWithScopeWhen = callBinaryWithScopeWhen(console.log)
// ... log as you calculate
const myFunction = pipe(
  logWithScopeWhen(
    id => id === 200,
    prop('id'),
    'the 200 id item'
  ),
  calculate
)
`
  },
  segment: {
    title: `Add binary side-effects with a curried object interface`,
    code: `
import {segment} from "xtrace"
import {prop} from "ramda"
// ... log the item whose id is 300 before calculating
const myFunction = pipe(
  value => segment({
    when: id => id === 300,
    what: prop('id'),
    call: console.log,
    tag: 'the 300 item',
    value
  })
  reduce(calculate, {})
)
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>Add side-effects to your functional pipelines.</Text>
)

const features = [
  {
    title: "Add unary side-effects",
    list: [() => <Code>callWithScopeWhen</Code>]
  },
  {
    title: "Add binary side-effects",
    list: [() => <Code>callBinaryWithScopeWhen</Code>]
  },
  {
    title: 'Add "object-style" side-effects',
    list: [() => <Code>segment</Code>]
  },
  {
    title: "Use sugar functions for even easier composition",
    list: [
      () => <Code>callWithScope</Code>,
      () => <Code>callWhen</Code>,
      () => <Code>call</Code>,
      () => <Code>callBinaryWithScope</Code>,
      () => <Code>callBinaryWhen</Code>,
      () => <Code>callBinary</Code>
    ]
  },
  {
    title: "Use the logging functions for even fewer keystrokes",
    list: [
      () => <Code>traceWithScopeWhen</Code>,
      () => <Code>inspect</Code>,
      () => <Code>trace</Code>,
      () => <Code>traceSegment</Code>
    ]
  }
]

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
