/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import Link from "../Link"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/handrail.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "handrail"

const samples = {
  basic: {
    title: "Use guideRail to handle multiple synchronous cases",
    code: `
import {guideRail, fold} from 'handrail'
import {pipe} from 'ramda' // or lodash/fp

// here are two potential error cases
const over21 = ({age}) => age > 20
const hasMoney = ({cash}) => cash - 5 >= 0

// and these are the cases we pass to the end, before folding
const growUp = (user) => 'Expected ' + user.name + ' to be 21!'
const getAJob = (user) => 'Expected ' + user.name + ' to have at least 5 dollars!'

// here's our original function, which has some errors in its assumptions
const bartenderOfIllRepute = (user) => {
  user.cash -= 5
  user.beverages = user.beverages || []
  user.beverages.push('beer')
  return user
}

// here's how we fix it with guideRail
const bartenderOfGoodRepute = pipe(
  guideRail(
    [
      // add safety for age!
      [over21, growUp],
      // add safety for cash!
      [hasMoney, getAJob]
      // add more!
    ],
    // alter the Either value
    bartenderOfIllRepute
  ),
  // this just pulls our value out from the Either
  fold(I, I)
)
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>
    Add safety to your synchronous functions via Eithers /{" "}
    <Link href="https://en.wikipedia.org/wiki/Logical_disjunction">
      logical disjunction
    </Link>
    .
  </Text>
)

const features = [
  {
    title: "Wrap a synchronous function with a single Either",
    list: [() => <Code>handrail</Code>]
  },
  {
    title: "Wrap a synchronous function with multiple Eithers",
    list: [() => <Code>multiRail</Code>, () => <Code>guideRail</Code>]
  },
  {
    title: "Process Left and Right results independently",
    list: [() => <Code>bimap</Code>]
  },
  {
    title: "Pull a value from out of the Either monad",
    list: [() => <Code>fold</Code>]
  }
]

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
