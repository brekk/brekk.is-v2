/** @jsx jsx */
/* @jsxFrag React.Fragment */
import React from "react" // eslint-disable-line no-unused-vars
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import Link from "../Link"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/phantomscript.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "phantomscript"

const samples = {
  encode: {
    title: "Convert stdin to zero-width characters",
    code: `
npx phantomscript -e "encode this string"
`
  },
  decode: {
    title: "Convert stdin from zero-width characters",
    code: `
npx phantomscript -e "encode this string" | npx phantomscript -d
`
  },
  api: {
    title: "Use the programmatic API",
    code: `
import {encode, decode} from 'phantomscript'
const MAGIC_KEY = encode('my-secret-key)
const logic = (input) => {
  if (MAGIC_KEY === encode(input)) {
    // do stuff?
  }
}
`
  }
}

const features = [
  {
    title: "Convert text into zero-width characters",
    list: [() => <Code>encode</Code>]
  },
  {
    title: "Convert zero-width characters back into text",
    list: [() => <Code>decode</Code>]
  },
  {
    title: "A highly modularized codebase",
    list: []
  }
]

const Paragraph1 = () => (
  <Text py={2}>
    Phantomscript was originally written about <Link href="https://github.com/jagracey/PhantomScript">here</Link>. This codebase simply encapsulates the ideas described in the original repo as an installable / runnable module.
  </Text>
)

const Paragraph2 = () => (
  <Text py={2}>
    This was done as an educational exercise but has potential for misuse. Use at your own risk. <Link href="https://medium.com/@umpox/be-careful-what-you-copy-invisibly-inserting-usernames-into-text-with-zero-width-characters-18b4e6f17b66">Relevant</Link> article explaining how zero-width characters can be used in an underhanded manner.
  </Text>
)

const Paragraph3 = () => (
  <Text py={2}>
    Learn more about Unicode <Link href="https://github.com/Wisdom/Awesome-Unicode">here</Link>.
  </Text>
)

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1, Paragraph2, Paragraph3]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
