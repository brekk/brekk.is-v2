/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { withProps, pure } from "recompose"
import { withTheme } from "emotion-theming"
import { NavLink } from "react-navi"
import { blem, jsx, Text } from "../view"
import { pipe } from "ramda"

import { ReactComponent as Logo } from "../logos/f-utility.svg"
import "../icons"

import Code from "../Code"
import { Package } from "./shared"

export const name = "f-utility"

const samples = {
  basic: {
    title: "Common uses",
    code: `
import {propOr, ap, pipe, map, curry} from 'f-utility'

const dataPipe = pipe(
  ap([
    propOr('', 'name'),
    propOr([], 'dependencies')
  ]),
  merge({cool: true}),
)
`
  }
}

const Paragraph1 = () => (
  <Text py={2}>
    <Code>f-utility</Code> is a functional utility toolbelt, originally modeled
    after <Code>ramda</Code>, but with a smaller bundled size.
  </Text>
)

const features = [
  {
    title: "Traditional parameter currying:",
    list: [() => <Code>curry</Code>]
  },
  {
    title: "Functional composition:",
    list: [() => <Code>pipe</Code>, () => <Code>compose</Code>]
  },
  {
    title: "Functional control flow:",
    list: [
      () => <Code>ifElse</Code>,
      () => <Code>when</Code>,
      () => <Code>unless</Code>
    ]
  },
  {
    title: "Functional data transformation:",
    list: [
      () => <Code>propOr</Code>,
      () => <Code>propEq</Code>,
      () => <Code>propSatisfies</Code>,
      () => <Code>pathOr</Code>,
      () => <Code>pathEq</Code>,
      () => <Code>pathSatisfies</Code>,
    ]
  }
]

const Component = ({ bem, name, theme, RoutedLink }) => (
  <Package
    name={name}
    bem={bem}
    theme={theme}
    RoutedLink={RoutedLink}
    paragraphs={[Paragraph1]}
    features={features}
    samples={samples}
    Logo={Logo}
  />
)
const decorator = pipe(
  withProps({ name, RoutedLink: NavLink, bem: blem("Package") }),
  withTheme,
  pure
)

export default decorator(Component)
