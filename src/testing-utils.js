/** @jsx jsx */
/* @jsxFrag React.Fragment */
import { jsx } from "./view"
import React from "react"

import { createPage, createSwitch, createMemoryNavigation } from "navi"

const pages = createSwitch({
  paths: {
    "/": createPage({ title: "testing", content: <span>cool</span> })
  }
})

export const addNavigation = () => {
  const navigation = createMemoryNavigation({ pages, url: "brekk.is" })
  return navigation
    .steady()
    .then(() => navigation)
    .catch(e => console.warn(e))
}
