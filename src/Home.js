/** @jsx jsx */
/* @jsxFrag React.Fragment */
import { css, jsx, blem } from "./view"
// eslint-disable-next-line
import React from "react"

import Footer from "./HomeFooter"
import Header from "./HomeHeader"
import OSS from "./OpenSource"
import OSSForks from "./OpenSourceForks"
import "./icons"

const Rule = ({ bem }) => (
  <hr
    className={bem("rule")}
    css={css`
      border: 0.5px solid rgba(0, 0, 0, 0.3);
    `}
  />
)

export const Home = ({ bem = blem("Home") }) => (
  <div
    className={bem()}
    css={css`
      transition: background-color 0.3s ease-out, color 0.3s ease-out;
      padding: 1rem;
      margin: 0;
    `}
  >
    <Header bem={bem} />
    <Rule bem={bem} />
    <OSS bem={bem} />
    <Rule bem={bem} />
    <OSSForks bem={bem} />
    <Rule bem={bem} />
    <Footer bem={bem} />
  </div>
)

export default Home
