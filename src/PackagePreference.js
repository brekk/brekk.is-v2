/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { pipe } from "ramda"
import { pure } from "recompose"
import { withThemeAndData, jsx, css, Text } from "./view"
import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome"

const PackagePreference = ({ preferYarnOrNPM, packageManager, bem }) => (
  <Text
    className={bem("choose-package-manager")}
    css={css`
      cursor: help;
      font-size: 0.75rem;
      margin-bottom: 1rem;
    `}
    onClick={preferYarnOrNPM}
    title="Choose yarn or npm, if you care."
  >
    <Icon
      icon={packageManager ? "circle" : "check-circle"}
      className={bem("icon", packageManager ? "yarn" : "npm")}
      css={css`
        margin-right: 0.5rem;
      `}
    />
    Prefer npm?
  </Text>
)

const hoc = pipe(
  withThemeAndData,
  pure
)

export default hoc(PackagePreference)
