/** @jsx jsx */
/* @jsxFrag React.Fragment */
// eslint-disable-next-line
import React from "react"
import { jsx, css } from "./view"

const Code = ({ children }) => (
  <code
    css={css`
      font-family: "Anonymous Pro", monospace;
    `}
  >
    {children}
  </code>
)
export default Code
