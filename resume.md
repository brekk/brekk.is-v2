# Brekk Bockrath


I am a software engineer with a deep focus on JavaScript. I am well-versed in providing a good user experience built with clean and simple software. I've served in a range of roles, from individual contributor to front-end technical lead of a small front-end team of eight. I enjoy mentoring and sharing knowledge, especially when working with active learners.

I learn quickly and I'm passionate about building both clean, data-driven interfaces and the frameworks that power them. I love anything that lives in the front-end or JavaScript. For the last few years I've been learning and trying to teach the patterns of functional programming &mdash; I am still a journeyman but I am very excited by the clarity, ease-of-maintenance and safety that FP has provided. I am fascinated by visualizations of complex systems and programs which can be run on themselves.

Here are some open-source modules I've published:
* `katsu-curry` for multiple different flavors of currying in JS
* `handrail` for adding safety to functional composition pipelines
* `entrust` to allow for delegatee-last tacit programming
* `blem` to facilitate the creation of BEM-style selectors
* `snang` for tacit programming in the command line.


[Find more on my site: ![brekk.is website](https://brekk.is/static/media/brekk-is.e7ba50d5.svg)](https://brekk.is)


## Experience

### April 2020 - present

Front-end Tech Lead @ _ebay_ / _mobile.de_; Berlin, Germany

- Worked as a frontend lead for a team of 6
- Maintained several legacy codebases built on top of jQuery + Angular
- Built a tool for parsing `soy` templates and automatically converting them to JSX / React components

### June 2019 - April 2020

Senior Front-end Engineer @ _Pincamp_; Berlin, Germany

- Worked as a front-end engineer for a team of 10
- Migrated a legacy codebase to `gatsby`, improving page load speed by 3x
- Created tooling to help automate builds and prevent foot-guns, using `lint-staged`
  and several small scripts built on top of `snang`

### October 2018 - June 2019

Front-end Developer @ _SF Networx_; Denver, CO

- Did contracting work for Long View Systems, including:
  - Built an internal invoicing tool using `electron-pdf` which allowed for generation of PDFs using Markdown and `scss`
  - Built an internal CMS tool using `express`, `axios` and `oauth`

### February 2018 - September 2018

Front-end Engineer @ _Golden_; San Francisco, CA

- Served as a front-end engineer in a small team of 10
- Added integration tests via `nightmare`
- Re-built old homepage using `rollup` and `x-ray`
- Added "Issues" feature for suggesting changes to the content of the page

### June 2015 - August 2017

Front-end Technical Lead @ _Fanatics_; San Mateo, CA

- Served as a front-end technical lead for a team of 8
- Built two versions of a framework for CMS tool, Lumen, for managing the
  content-authoring of white-label sports sites, including layouts, components
  and experimentation.
  _Technologies_: `react`; `redux`; `mobx`; `express`; `fluture`;
  `ramda`; `scss`;
- Worked as a re-platforming engineer and augmented views to support a
  new style / theme
  _Technologies_: `jQuery`; `postcss`; `scss`

### Dec. 2013 – June 2015

Front-end Engineer @ _Watchwith_; San Francisco, CA.

- Served as technical lead for a 2-person front-end team
- Built software for a second-screen application to keep broadcasters "in-sync" (using an external API)
  with television-based data, including live broadcasts.
  _Technologies_: `JavaScript`; `HTML5`; `<video>`; `CSS` _Integrations_: `ThePlatform (Video)`;
  `Telescope (Voting)`

### Jan. 2010 – 2013

_Independent Software Engineering Contractor_
Boulder, CO.

- For Cardinal Peak, completed functional enhancements to a television-based
  user navigation system for Blockbuster/Samsung.
  _Technologies_: `JavaScript`; `HTML5`
- For Mischievous Grin, developed educational game framework and proof of
  concept.
  _Technologies_: `MongoDB`; `node`; `express`; `grunt`; `Mongoose`;
  `JSON`; `HTML5 Canvas`; `git`
- For Apple: Developed and implemented an automated image quality report
  generator.
  _Technologies_: `PHP`; `CSS`; `XHTML`
- For Imatest: Designed and developed server-side, database and UI
  components for an image quality analysis system.
  _Technologies_: `PHP`;
  `SOAP`; `MySQL`; `HTML5`; `CSS`; `XML`; `JSON`; `jQuery`; `scss`

### Jan. 2012 – Aug. 2012

Software Engineer @ _Upsync_; Boulder, CO.

- Designed and implemented a client-side application for creating and editing
  ad-hoc HTML forms.
  _Technologies_: JavaScript; jQuery; HTML
- Helped develop and maintain a CMS web application which allowed syncing
  between servers and remote iOS devices.
  _Technologies_: `XHTML`; `HTML5`;
  `jQuery`; `SVN`; `CSS`; `sass`; `MySQL`; `WebSQL`

## Education

_Bachelor of Fine Arts_, 2009
University of Auckland, New Zealand
Emphasis: Computer-based graphic design; Interactive time-based media & Film
