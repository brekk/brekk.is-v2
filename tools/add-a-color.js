const fs = require("fs")
const path = require("path")

const { trace } = require("xtrace")
const chalk = require("chalk")
const F = require("fluture")
const { e0 } = require("entrust")
const yargsParser = require("yargs-parser")
const align = require("wide-align")
const slug = require("slugify")
const hello = require("hello-color").default
const { map, pipe, random, curry } = require("f-utility")
const countries = require("country-list").getNames()
const grabHex = random.wordSource("0123456789abcdef".split(""))
const prepend = curry((a, b) => a + b)
const randomColor = () =>
  pipe(
    grabHex,
    prepend("#")
  )(6)

const truncateBefore = curry((delim, str) => {
  const index = str.indexOf(delim)
  return index > -1 ? str.substr(0, index) : str
})

const getCountry = pipe(
  () => random.pick(countries),
  truncateBefore("people"),
  truncateBefore(`,`),
  truncateBefore(`(`),
  slug,
  e0("toLowerCase")
)

const config = yargsParser(process.argv.slice(2), {
  alias: {
    color: ["c"],
    name: ["n"]
  }
})

const { name = getCountry(), color = randomColor() } = config
const colors = hello(color)
const padify = curry((n, str) =>
  [align.center("", n), align.center(str, n), align.center("", n)].join("\n")
)

pipe(
  JSON.stringify,
  padify(100),
  chalk.hex(colors.base),
  chalk.bgHex(colors.color),
  console.log
)({ name, base: colors.base, color: colors.color })
