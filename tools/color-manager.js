const fs = require("fs")
const path = require("path")

const F = require("fluture")
const yargsParser = require("yargs-parser")
const hello = require("hello-color").default
const { e0 } = require("entrust")
const { trace } = require("xtrace")
const slug = require("slugify")

const {
  curry,
  filter,
  I,
  reject,
  fork,
  pipe,
  map,
  join,
  split,
  random
} = require("f-utility")
const countries = require("country-list").getNames()
const truncateAfter = curry((x, y) => {
  const i = y.indexOf(x)
  return ~i ? y.substr(0, i) : y
})
const getCountry = pipe(
  () => random.pick(countries),
  e0("toLowerCase"),
  truncateAfter(","),
  truncateAfter(" and"),
  truncateAfter("("),
  slug
)

const prepend = curry((a, b) => a + b)

const addColor = z =>
  pipe(
    random.wordSource(`0123456789abcdef`.split(``)),
    prepend("#"),
    y => z.concat([[getCountry(), y, hello(y).color]])
  )(6)

const readFile = x => F.encaseN(fs.readFile)(x, "utf8").map(z => z.toString())
const writeFile = x => F.encaseN(fs.writeFile)(x, "utf8")

const manager = () => {
  const options = {}

  const parse = x => yargsParser(x, options)

  const config = parse(process.argv.slice(2))
  const { _: instructions } = config
  const [instruction, a, b] = instructions

  const commands = [`rename`, `create`, `delete`, `flip`]

  if (!commands.includes(instruction))
    return console.log(instruction + " is not a valid subcommand.")

  const command = curry((i, x) => {
    if (i === "delete") return reject(([j, k, l]) => j === a, x)
    if (i === "rename")
      return map(([j, k, l]) => (j === a ? [b, k, l] : [j, k, l]), x)
    if (i === "flip")
      return map(([j, k, l]) => (j === a ? [j, l, k] : [j, k, l]), x)
    if (i === "create") return addColor(x)
  })

  return pipe(
    readFile,
    map(
      pipe(
        split("\n"),
        filter(I),
        map(split(",")),
        command(instruction),
        map(join(",")),
        join("\n")
      )
    ),
    // chain(writeFile(path.resolve(__dirname, 'colors.csv'))),
    fork(console.warn, console.log)
  )(path.resolve(__dirname, "colors.csv"))
}

manager()
