module.exports = {
  spain: {
    fore: "#002200",
    back: "#bb3256"
  },
  tajikistan: {
    fore: "#b0c04f",
    back: "#4c3cad"
  },
  hungary: {
    fore: "#002e00",
    back: "#c7289a"
  },
  eswatini: {
    fore: "#e9002c",
    back: "#03eac7"
  },
  northern: {
    fore: "#e40e02",
    back: "#17e8f9"
  },
  portugal: {
    fore: "#3a1900",
    back: "#3967c9"
  },
  nepal: {
    fore: "#002f12",
    back: "#975e84"
  },
  ethiopia: {
    fore: "#ebffff",
    back: "#ad8d5f"
  },
  trinidad: {
    fore: "#002d66",
    back: "#ca530b"
  },
  mali: {
    fore: "#4f3c79",
    back: "#b3c689"
  },
  serbia: {
    fore: "#41462e",
    back: "#c1bcd4"
  },
  honduras: {
    back: "#ac0b31",
    fore: "#57fdd1"
  },
  bhutan: {
    fore: "#001326",
    back: "#a64025"
  },
  french: {
    fore: "#005149",
    back: "#f85f6b"
  },
  vanuatu: {
    fore: "#86c727",
    back: "#7635d4"
  },
  brazil: {
    fore: "#003a00",
    back: "#ac50d5"
  },
  togo: {
    fore: "#331a2c",
    back: "#53735d"
  },
  somalia: {
    fore: "#8f222d",
    back: "#36a8a6"
  },
  russian: {
    fore: "#633549",
    back: "#9fcdb9"
  },
  micronesia: {
    fore: "#674a43",
    back: "#9bb8bf"
  },
  libya: {
    fore: "#001d00",
    back: "#7a44cf"
  },
  sint: {
    fore: "#89282d",
    back: "#3fa6a7"
  },
  pitcairn: {
    fore: "#321400",
    back: "#3c61c7"
  },
  lucia: {
    fore: "#bccfbb",
    back: "#402d41"
  },
  comoros: {
    fore: "#d01077",
    back: "#27e082"
  },
  costa: {
    fore: "#7a3400",
    back: "#4999e4"
  },
  estonia: {
    fore: "#bb350e",
    back: "#48cdf5"
  },
  finland: {
    fore: "#9dbd49",
    back: "#5f3fb3"
  },
  sahara: {
    fore: "#2a1541",
    back: "#d8ecc0"
  },
  suriname: {
    fore: "#004c2d",
    back: "#c27498"
  },
  belize: {
    fore: "#5c0080",
    back: "#30930e"
  },
  isle: {
    fore: "#90e75f",
    back: "#6c159d"
  },
  kyrgyzstan: {
    fore: "#bcf7be",
    back: "#40053e"
  },
  poland: {
    back: "#3931e5",
    fore: "#ccd11d"
  },
  greenland: {
    fore: "#b5d6d0",
    back: "#47262c"
  },
  tokelau: {
    fore: "#003400",
    back: "#c54187"
  },
  san: {
    fore: "#002b00",
    back: "#b1448a"
  },
  africa: {
    fore: "#853999",
    back: "#7dc969"
  },
  solomon: {
    fore: "#edc2d9",
    back: "#0f3a23"
  },
  reunion: {
    fore: "#98c613",
    back: "#6436e6"
  },
  tuvalu: {
    fore: "#c5cc2c",
    back: "#3630cf"
  },
  palestine: {
    fore: "#00382f",
    back: "#dd3c4d"
  },
  niger: {
    fore: "#cec000",
    back: "#2d3cfe"
  },
  cocos: {
    fore: "#271e00",
    back: "#5a669f"
  },
  kitts: {
    fore: "#c3b7ec",
    back: "#394510"
  },
  kuwait: {
    fore: "#c7fc85",
    back: "#350077"
  },
  anguilla: {
    fore: "#3a1d00",
    back: "#3b63e2"
  },
  cook: {
    back: "#313dfe",
    fore: "#d5c505"
  },
  guatemala: {
    fore: "#bfaa68",
    back: "#3d5294"
  },
  burkina: {
    fore: "#2708cc",
    back: "#e0fc36"
  },
  barbados: {
    fore: "#5e000d",
    back: "#168085"
  },
  pakistan: {
    fore: "#450de7",
    back: "#bff91c"
  },
  curacao: {
    fore: "#58e426",
    back: "#a318d4"
  },
  gambia: {
    fore: "#9b373d",
    back: "#67cbc5"
  },
  china: {
    fore: "#7f1353",
    back: "#83f1af"
  },
  andorra: {
    fore: "#aa19d1",
    back: "#51e02a"
  },
  salvador: {
    fore: "#121436",
    back: "#f0edcc"
  },
  panama: {
    fore: "#2a5582",
    back: "#d9ad80"
  },
  christmas: {
    fore: "#bc09cc",
    back: "#3feb2f"
  },
  thailand: {
    fore: "#3c446b",
    back: "#c6be97"
  },
  vincent: {
    fore: "#180ef0",
    back: "#f8f713"
  },
  mozambique: {
    fore: "#723b3f",
    back: "#67a29e"
  },
  switzerland: {
    fore: "#40004a",
    back: "#1e7c1a"
  },
  argentina: {
    fore: "#6d24c9",
    back: "#96df39"
  },
  belgium: {
    fore: "#a8e119",
    back: "#531be0"
  },
  luxembourg: {
    fore: "#002b00",
    back: "#a02bee"
  },
  aruba: {
    fore: "#00652f",
    back: "#f483bc"
  },
  denmark: {
    fore: "#38582a",
    back: "#b392c2"
  },
  brunei: {
    fore: "#c2004d",
    back: "#01cf85"
  },
  states: {
    fore: "#00442f",
    back: "#dc5672"
  },
  mongolia: {
    fore: "#91f35d",
    back: "#6b099f"
  },
  france: {
    fore: "#d0ca27",
    back: "#2b32d4"
  },
  moldova: {
    fore: "#af0055",
    back: "#1ec082"
  },
  morocco: {
    fore: "#296647",
    back: "#d99cbb"
  },
  paraguay: {
    fore: "#bf0069",
    back: "#10d06e"
  },
  faroe: {
    fore: "#005e8e",
    back: "#f78f5e"
  },
  palau: {
    fore: "#394ea4",
    back: "#b6a44c"
  },
  turkey: {
    fore: "#1a277f",
    back: "#eadb83"
  },
  korea: {
    fore: "#a320cd",
    back: "#5fe435"
  },
  sierra: {
    fore: "#633f75",
    back: "#7fa56d"
  },
  fiji: {
    fore: "#931f80",
    back: "#46b85c"
  },
  papua: {
    fore: "#324d5a",
    back: "#ac9083"
  },
  grenada: {
    fore: "#1e5aa2",
    back: "#e7a860"
  },
  mayotte: {
    fore: "#87aec6",
    back: "#754e36"
  },
  mexico: {
    fore: "#b42400",
    back: "#2abcf8"
  },
  sweden: {
    fore: "#063e00",
    back: "#975feb"
  },
  lao: {
    fore: "#0d16a2",
    back: "#ffec60"
  },
  kiribati: {
    fore: "#002e1d",
    back: "#d62744"
  },
  venezuela: {
    fore: "#95e807",
    back: "#6614ed"
  },
  yemen: {
    fore: "#ada6bd",
    back: "#4f563f"
  },
  mauritius: {
    fore: "#87ba97",
    back: "#7b486b"
  },
  mauritania: {
    fore: "#b31171",
    back: "#50f791"
  },
  guyana: {
    fore: "#1d2800",
    back: "#6f4ef4"
  },
  austria: {
    fore: "#d5d6f0",
    back: "#27260c"
  },
  caledonia: {
    fore: "#3be82d",
    back: "#bf14ce"
  },
  islands: {
    fore: "#1c0940",
    back: "#4e6c28"
  },
  slovakia: {
    fore: "#75ede0",
    back: "#860f1c"
  },
  belarus: {
    fore: "#41e1c8",
    back: "#b91b34"
  },
  ireland: {
    fore: "#001b00",
    back: "#a9357b"
  },
  romania: {
    fore: "#314bda",
    back: "#c5af1f"
  },
  iran: {
    fore: "#2f1b42",
    back: "#5b7546"
  },
  bouvet: {
    fore: "#230f21",
    back: "#def3e0"
  },
  czech: {
    fore: "#afc250",
    back: "#4d3aac"
  },
  greece: {
    fore: "#d7cd00",
    back: "#242ffc"
  },
  nigeria: {
    fore: "#3133fc",
    back: "#d5cf07"
  },
  american: {
    fore: "#00613b",
    back: "#e885ae"
  },
  sudan: {
    fore: "#9ac1aa",
    back: "#623b52"
  },
  kingdom: {
    fore: "#004b00",
    back: "#cc5bd5"
  },
  jordan: {
    fore: "#814700",
    back: "#6aa7f9"
  },
  timor: {
    fore: "#b92335",
    back: "#33c8bd"
  },
  arab: {
    fore: "#fbc28e",
    back: "#003a6e"
  },
  iraq: {
    fore: "#005cbc",
    back: "#fa9b3a"
  },
  botswana: {
    fore: "#683b01",
    back: "#5f95cd"
  },
  qatar: {
    fore: "#85e806",
    back: "#7614ee"
  },
  philippines: {
    fore: "#764a38",
    back: "#7dabbd"
  },
  chile: {
    fore: "#003531",
    back: "#de343f"
  },
  georgia: {
    fore: "#4fedb9",
    back: "#ac0f43"
  },
  cameroon: {
    fore: "#531ca1",
    back: "#afe661"
  },
  congo: {
    fore: "#31213a",
    back: "#617658"
  },
  madagascar: {
    fore: "#1e119a",
    back: "#e7f168"
  },
  australia: {
    fore: "#001944",
    back: "#7c632a"
  },
  lebanon: {
    fore: "#281627",
    back: "#566d58"
  },
  djibouti: {
    fore: "#512987",
    back: "#699732"
  },
  lesotho: {
    fore: "#860000",
    back: "#0696b9"
  },
  zealand: {
    fore: "#1b33bc",
    back: "#929906"
  },
  ecuador: {
    fore: "#096017",
    back: "#d386d1"
  },
  guam: {
    fore: "#610078",
    back: "#189506"
  },
  malaysia: {
    fore: "#004f00",
    back: "#f63cc7"
  },
  puerto: {
    fore: "#9e10cf",
    back: "#65f930"
  }
}
