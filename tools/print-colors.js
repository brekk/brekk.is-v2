const fs = require("fs")
const path = require("path")
const chalk = require("chalk")
const F = require("fluture")
const { e0 } = require("entrust")
const align = require("wide-align")
const { split, pipe, join, entries, curry, map, fork } = require("f-utility")
const { trace } = require("xtrace")

const paddify = curry((n, str) => {
  const padded = align.center(str, n)
  const x = align.center("", n)
  return `${x}\n${padded}\n${x}`
})

const readFile = x => F.encaseN(fs.readFile)(x, "utf8").map(x => x.toString())

const print = pipe(
  split(`\n`),
  map(split(",")),
  map(
    ([k, fore, back]) =>
      k &&
      pipe(
        paddify(25),
        chalk.hex(fore),
        chalk.bgHex(back)
      )(k)
  ),
  join("\n")
)

pipe(
  readFile,
  map(print),
  fork(console.warn, console.log)
)(path.resolve(__dirname, process.argv.slice(2)[0] || "colors.csv"))
