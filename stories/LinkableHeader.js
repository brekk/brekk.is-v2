import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import LinkableHeader from "../src/LinkableHeader"

stories.add("LinkableHeader", () => (
  <Render>
    <LinkableHeader>check out my website!</LinkableHeader>
  </Render>
))
