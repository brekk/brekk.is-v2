import React from "react"
import blem from "blem"

import { stories, Render } from "./utils"
import Home from "../src/Home"
import Link from "../src/Link"

stories.add("Home", () => (
  <Render>
    <Home bem={blem("Home")} RoutedLink={Link} />
  </Render>
))
