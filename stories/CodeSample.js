import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import CodeSample from "../src/CodeSample"

stories.add("CodeSample", () => (
  <Render>
    <CodeSample bem={blem("App")} title="this is a linked header!">{`
pipe(
  split(' '),
  map(x => x[0].toUpperCase() + x.substr(1)),
  join(' ')
)('whatever you want')
    `}</CodeSample>
  </Render>
))
