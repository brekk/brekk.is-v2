import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import Code from "../src/CodeInstall"

stories.add("CodeInstall", () => (
  <Render>
    <Code bem={blem("App")} pkg="butts" />
  </Render>
))

stories.add("CodeInstall - npm", () => (
  <Render>
    <Code bem={blem("App")} pkg="butts" packageManager={false} />
  </Render>
))
