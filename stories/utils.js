import { storiesOf } from "@storybook/react"
import React from "react"
import { createBrowserNavigation } from "navi"
import NavProvider from "react-navi"
import { ThemeProvider } from "emotion-theming"
import pages from "../src/pages"

export const stories = storiesOf("Brekk.is", module)
export const navigation = createBrowserNavigation({ pages })
navigation.steady()

const theme = {
  name: "butts",
  colors: {
    fore: "#000000",
    back: "#ffffff"
  }
}
export const Render = ({ children: x }) => (
  <ThemeProvider theme={theme}>{x}</ThemeProvider>
)
