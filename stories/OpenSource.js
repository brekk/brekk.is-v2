import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import OpenSource from "../src/OpenSource"
import Link from "../src/Link"

const nav = {}

stories.add("OpenSource", () => (
  <Render>
    <OpenSource bem={blem("App")} RoutedLink={Link} />
  </Render>
))
