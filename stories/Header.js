import React from "react"
import blem from "blem"

import { stories, Render } from "./utils"
import Header from "../src/HomeHeader"

stories.add("Header", () => (
  <Render>
    <Header bem={blem("App")} />
  </Render>
))
