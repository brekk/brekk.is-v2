import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import LoadingBar from "../src/LoadingBar"

stories.add("LoadingBar", () => (
  <Render>
    <LoadingBar isBusy={true} />
  </Render>
))
