import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import PackagePreference from "../src/PackagePreference"

stories.add("PackagePreference", () => (
  <Render>
    <PackagePreference bem={blem("App")} />
  </Render>
))
