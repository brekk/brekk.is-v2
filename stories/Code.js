import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import Code from "../src/Code"

stories.add("Code", () => (
  <Render>
    <Code bem={blem("App")}>npx snang</Code>
  </Render>
))
