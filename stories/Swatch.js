import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import Swatch from "../src/Swatch"

stories.add("Swatch", () => (
  <Render>
    <Swatch
      theme={{ colors: { fore: "#ff0", back: "#00f" } }}
      bem={blem("App")}
    />
  </Render>
))
stories.add("Swatch - index back", () => (
  <Render>
    <Swatch
      index="back"
      theme={{ colors: { fore: "#ff0", back: "#00f" } }}
      bem={blem("App")}
    />
  </Render>
))
