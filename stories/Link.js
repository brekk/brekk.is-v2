import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import Link from "../src/Link"

stories.add("Link", () => (
  <Render>
    <Link>check out my website!</Link>
  </Render>
))
