import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import Footer from "../src/HomeFooter"

stories.add("Footer", () => (
  <Render>
    <Footer bem={blem("App")} />
  </Render>
))
stories.add("Footer - with previousSwatch", () => (
  <Render>
    <Footer bem={blem("App")} previousSwatch={[1, 2]} />
  </Render>
))
