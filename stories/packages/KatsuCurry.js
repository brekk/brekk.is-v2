import React from "react"
import blem from "blem"

import { Render, stories } from "../utils"
import Link from "../../src/Link"
import { ThemeProvider } from "emotion-theming"
import KatsuCurry from "../../src/packages/KatsuCurry"

const A = props => <a {...props}>{props.children}</a>

stories.add("Packages - KatsuCurry", () => (
  <Render>
    <KatsuCurry RoutedLink={A} />
  </Render>
))
