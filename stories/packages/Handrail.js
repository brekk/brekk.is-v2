import React from "react"
import blem from "blem"

import { Render, stories } from "../utils"
import Link from "../../src/Link"
import { ThemeProvider } from "emotion-theming"
import Handrail from "../../src/packages/Handrail"

const A = props => <a {...props}>{props.children}</a>

stories.add("Packages - Handrail", () => (
  <Render>
    <Handrail RoutedLink={A} />
  </Render>
))
