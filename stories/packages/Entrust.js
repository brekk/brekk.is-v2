import React from "react"
import blem from "blem"

import { Render, stories } from "../utils"
import Link from "../../src/Link"
import { ThemeProvider } from "emotion-theming"
import Entrust from "../../src/packages/Entrust"

const A = props => <a {...props}>{props.children}</a>

stories.add("Packages - Entrust", () => (
  <Render>
    <Entrust RoutedLink={A} />
  </Render>
))
