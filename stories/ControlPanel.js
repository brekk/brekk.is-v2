import React from "react"
import blem from "blem"

import { stories, Render } from "./utils"
import ControlPanel from "../src/ControlPanel"

stories.add("ControlPanel", () => (
  <Render>
    <ControlPanel bem={blem("App")} />
  </Render>
))
