import React from "react"
import blem from "blem"

import { Render, stories } from "./utils"
import { ThemeProvider } from "emotion-theming"
import { StyledError as RoutingError } from "../src/RoutingError"

class NotFoundError extends Error {
  constructor(x) {
    super(x)
  }
}
NotFoundError.prototype.name = "NotFoundError"

stories.add("RoutingError", () => (
  <Render>
    <RoutingError
      error={new NotFoundError("URL not found: /fucking-a-duck/")}
    />
  </Render>
))
